module QA
  module Page
    module Components
      module Navbar
        def click_login_link
          click_element(:log_in_link)
        end

        def click_account_dropdown_link
          click_element(:account_dropdown_link, wait:1)
        end

        def click_logout_link
          click_element(:log_out_link, wait: 1)
        end

        def click_tokens_link
          click_element(:tokens_link)
        end

        def click_users_link
          click_element(:users_link)
        end

        def has_no_account_menu?(wait: Capybara.default_max_wait_time)
          has_no_element?(:account_dropdown_link, wait: wait)
        end

        def has_account_menu?(wait: Capybara.default_max_wait_time)
          has_element?(:account_dropdown_link, wait: wait)
        end

        def signed_in?
          has_account_menu?(wait: 0)
        end

        def signed_in_as_user?(user)
          return false if has_no_account_menu?

          within_account_menu do
            has_element?(:user_profile_link, text: /#{user.name}/)
          end
        end

        def within_account_menu(&block)
          within_element(:account_menu_dropdown, &block)
        end
      end
    end
  end
end
