module QA
  module Page
    class Signup < Base
      def fill_name_field(name)
        fill_element(:name_field, name)
      end

      def fill_email_field(email)
        fill_element(:email_field, email)
      end

      def fill_password_field(password)
        fill_element(:password_field, password)
      end

      def fill_password_confirmation_field(password_confirmation)
        fill_element(:password_confirmation_field, password_confirmation)
      end

      def click_signup_button
        click_element(:sign_up_button)
      end
    end
  end
end
