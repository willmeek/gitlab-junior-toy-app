module QA
  module Page
    module User
      class Show < Base
        def click_next_paginate_link
          within_element(:will_paginate_content) do
            click_link('Next →')
          end
        end

        def click_delete_for_user(user_email)
          within_element(:user_content, text: user_email) do
            accept_confirm do
              click_element(:delete_user_link)
            end
          end
        end
      end
    end
  end
end
