# GitLab Junior UI Documentation

Welcome to the GitLab Junior UI documentation!

If you are looking for the REST API documentation, [head over here](api/index.md).

This application supports the following features:

## Users

#### Create a new user
Click the "Sign up now!" button on the landing page to create a user with the following
information:

| Field                  | Required? |
|:-----------------------|:----------|
| Name                   | Yes       |
| Email Address (Unique) | yes       |
| Password               | yes       |
| Password Confirmation  | yes       |

#### List Users
Sign in and use the navigation bar to go to Users list page.

#### View User Profile
Sign in and use the navigation bar to go to Users list page. Click a user to view their
profile information.

#### Update a user
Use the navigation bar to go to Account > Settings and update a user's information.

#### Delete a user (admin only)
Use the navigation bar to go to Users. Click `delete` next to a user to delete it.
Only an admin users can delete other users. Admin users cannot delete themselves.

Deleting a user also deletes all of it's groups and subgroups

## Session

#### Sign in
Use the navigation bar to go to the Log in page. Enter the following information to login:

| Field    | Required? |
|:---------|:----------|
| Email    | Yes       |
| Password | Yes       |

#### Sign out
Use the navigation bar to go to Account > Log out.

## Personal Access Tokens
A personal access token can be used to access the application resources via the API.

#### Create a new Personal Access Token
Sign in and use the navigation bar to go to Account > Tokens.
Following information is required to create a personal access token:

| Field                  | Required? |
|:-----------------------|:----------|
| Name                   | Yes       |

#### Delete a Personal Access Token
Sign in and use the navigation bar to go to Account > Tokens.
Click the delete icon next to a token to delete it.

## Groups
A group represents a unit in an organization. A group has an owner and can have multiple
subgroups.

#### List Groups and Subgroups
Sign in to view the user's groups and subgroups. Each group is listed with its full path to
the parent and its description.

To view another user's groups, use the navigation bar to go to the Users list page.
Click a user to view their groups and subgroups.

#### Create a new Top Level Group
Sign in and and click "New Top Level Group" to create a group with the following information:

| Field       | Required? |
|:------------|:----------|
| Name        | Yes       |
| Description | Yes       |

#### Create a new Subgroup
Sign in and on the landing page, click any existing group. Click the "New Subgroup" button. 
Use the subgroup form to create new subgroup with the following information:

| Field       | Required? |
|:------------|:----------|
| Name        | Yes       |
| Description | Yes       |

Only the group's owner can add a subgroup to a group.

#### Update a Group/Subgroup
On the landing page or on a group's show page, click the edit icon next to a group to update
the information that group. Only the group's owner or an admin can update a group's information.

#### Delete a Group/Subgroup
On the landing page or on a group's show page, click the delete icon next to a group to delete
that group. Only the group's owner or an admin can delete a group.

Deleting a group also deletes any subgroups under the group.
