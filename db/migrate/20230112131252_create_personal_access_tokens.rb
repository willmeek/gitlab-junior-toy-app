class CreatePersonalAccessTokens < ActiveRecord::Migration[7.0]
  def change
    create_table :personal_access_tokens do |t|
      t.string :name
      t.string :value
      t.references :user, null: false, foreign_key: true
      t.timestamps
    end

    add_index :personal_access_tokens, :value
  end
end
